module.exports = {
  paths: {
    markup: {
      src: './src/*.pug',
      all: 'src/**/*.pug',
      dest: './dist'
    },
    css: {
      src: './assets/styl/main.styl',
      critical: './assets/styl/critical.styl',
      all: 'assets/styl/**/*.styl',
      dev: './.tmp/css',
      dest: './dist/assets/css',
      vendorSrc: './assets/css/vendor/*.js',
      vendorDev: './.tmp/css/vendor',
      vendorDest: './dist/assets/css/vendor'
    },
    js: {
      src: './assets/js/main.js',
      all: 'assets/js/**/*.js',
      dev: './.tmp/js',
      dest: './dist/assets/js',
      vendorSrc: './assets/js/vendor/*.js',
      vendorDev: './.tmp/js/vendor',
      vendorDest: './dist/assets/js/vendor'
    },
    img: {
      src: 'assets/img/*',
      dest: './dist/assets/img'
    },
    fonts: {
      src: './assets/fonts/*',
      dest: './dist/assets/fonts'
    },
    data: {
      src: './rev-manifest.json'
    }
  },
  server: {
    proxy: 'localhost:8888/',
    basedir: './dist',
    filetype: 'html'
  }
};

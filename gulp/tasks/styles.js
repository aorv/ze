var config    = require('../config'),
    gulp      = require('gulp'),
    plugins   = require('gulp-load-plugins')({ overridePattern: false, pattern: ['*']}),
    inlineSource = require('gulp-inline-source'),
    error     = require('../error-handler.js');

gulp.task('styles', function () {
    return gulp.src(config.paths.css.src)
    .pipe(plugins.plumber({errorHandler: error.handler}))
    .pipe(plugins.stylus({
        use: [plugins.jeet(), plugins.nib(), plugins.rupture()]
    }))
    .pipe(gulp.dest(config.paths.css.dev))
});

gulp.task('styles:minify', function () {
    return gulp.src([config.paths.css.vendorSrc, config.paths.css.src])
    .pipe(plugins.plumber({errorHandler: error.handler}))
    .pipe(plugins.stylus({
        use: [plugins.jeet(), plugins.nib(), plugins.rupture()],
        compress: true
    }))
    .pipe(plugins.concat('main.css'))
    .pipe(plugins.cleanCss())
    .pipe(plugins.rename({suffix: '.min'}))
    .pipe(gulp.dest(config.paths.css.dest))
    .pipe(plugins.browserSync.stream())
});

gulp.task('styles:critical', function () {
    return gulp.src(config.paths.css.critical)
    .pipe(plugins.plumber({errorHandler: error.handler}))
    .pipe(plugins.stylus({
        use: [plugins.jeet(), plugins.nib(), plugins.rupture()]
    }))
    .pipe(gulp.dest(config.paths.css.dest))
    .pipe(plugins.browserSync.stream())
});

gulp.task('styles:inline', ['styles:critical'], function() {
    return gulp.src(config.paths.markup.dest + '/index.html')
    .pipe(inlineSource())
    .pipe(gulp.dest(config.paths.markup.dest));
});

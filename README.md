# README #

### Setup ###

```
git clone https://aorv@bitbucket.org/aorv/ze.git
cd ze
npm install
```

### Run Dev Build & Server ###

```
gulp
```

### Build for Production (minify/cachebust) ###

```
gulp production
```
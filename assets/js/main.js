window.ze = window.ze || {};

ze.getYear = {
  selector: '.js-year',
  init: function() {
    return new Date().getFullYear();
  }
};

u(ze.getYear.selector).text(ze.getYear.init());

ze.scrollLink = {
  trigger: '.js-link',
  getTarget: function(el) {
    var target = u(el).data('target');
    return target;
  },
  go: function(el) {
    u(el).scroll();
  }
}

u(ze.scrollLink.trigger).on('click', function() {
  var targetEl = ze.scrollLink.getTarget(this);
  ze.scrollLink.go(targetEl);
});

ze.stickyHeader = {
  el: '.js-sticky-header',
  animateClass: 'animate',
  elDistanceTop: function() {
    var distance = u('body').size();
    return distance.y;
  },
  animateIn: function() {
    u(this.el).addClass(this.animateClass);
  },
  animateOut: function() {
    u(this.el).removeClass(this.animateClass);
  }
}

window.addEventListener('scroll', function() {
  if (ze.stickyHeader.elDistanceTop() <= -300) {
    ze.stickyHeader.animateIn();
  } else {
    ze.stickyHeader.animateOut();
  }
});

window.addEventListener('load', function() {
  u('.hero').addClass('animate');
  u('header').addClass('show-bg');
  u('footer').removeClass('show-bg')
});